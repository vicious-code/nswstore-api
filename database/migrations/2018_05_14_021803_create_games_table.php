<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('buyitnow');
            $table->boolean('buyonline');
            $table->float('ca_price')->nullable();
            $table->boolean('digitaldownload');
            $table->float('eshop_price')->default(0.00);
            $table->boolean('free_to_start');
            $table->string('front_box_art');
            $table->string('game_code');
            $table->string('game_id');
            $table->string('nsuid')->default('-');
            $table->string('number_of_players');
            // $table->timestamp('real_release_date');
            $table->timestamp('release_date');
            $table->string('slug');
            $table->string('system');
            $table->string('title');
            $table->string('video_link')->nullable();
            $table->float('sale_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
