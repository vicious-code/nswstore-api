<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $guarded = ['id'];

    protected $dates = [
        'created_at',
        'updated_at',
        'release_date',
        'sales_end',
    ];

    // protected $casts = [
    //     'rating' => 'float(8,2)',
    // ];

    public function setBuyitnowAttribute($value)
    {
        $this->attributes['buyitnow'] = (int)$value;
    }
    public function setDigitaldownloadAttribute($value)
    {
        $this->attributes['digitaldownload'] = (int)$value;
    }

    public function setBuyonlineAttribute($value)
    {
        $this->attributes['buyonline'] = (int)$value;
    }
    public function setFreeToStartAttribute($value)
    {
        $this->attributes['free_to_start'] = (int)$value;
    }
    public function setRatingAttribute($value)
    {
        $this->attributes['rating'] = (float)$value;
    }

    public function getRatingAttribute($value)
    {
        return number_format($value, 2);
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function favourite()
    {
        $userId = \Auth::user()->id;
        $res = \App\Favourite::where('game_id', $this->id)->where('user_id', $userId)->get()->toArray();

        return (empty($res)) ? false : true;
    }

    public function favourites()
    {
        $userId = \Auth::user()->id;
        $res = \App\Favourite::where('user_id', $userId)->get();

        return (empty($res)) ? false : true;
    }

    public function yourRating()
    {
        $userId = \Auth::user()->id;
        $res = \App\Rating::where('game_id', $this->id)->where('user_id', $userId)->first();

        return number_format($res['rating'], 2);
    }

}
