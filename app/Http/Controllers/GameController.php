<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Resources\Games as GameResource;
use Illuminate\Http\Request;
use Carbon\Carbon;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return GameResource::collection(Game::orderBy('release_date', 'DESC')->paginate(40));
        // return GameResource::collection(Game::where('sale_price', '>', 0)->paginate(40));
        // return GameResource::collection(Game::where('release_date', '<=', Carbon::today())->orderBy('release_date', 'DESC')->paginate(40));
    }

    public function fullGames()
    {
        return GameResource::collection(Game::all());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function recentGames()
    {
        // return GameResource::collection(Game::where('sale_price', '>', 0)->paginate(40));
        return GameResource::collection(Game::where('release_date', '<=', Carbon::today())->orderBy('release_date', 'DESC')->paginate(40));
    }

    public function saleGames()
    {
        return GameResource::collection(Game::where('sale_price', '>', 0)->paginate(40));
    }
    
    public function comingSoonGames()
    {
        return GameResource::collection(Game::where('release_date', '>', Carbon::today())->orderBy('release_date', 'ASC')->paginate(40));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $game = Game::create($request->all());

        return response()->json($game, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        return new GameResource(Game::find($game->id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game)
    {
        $game->update($request->all());
        
        return response()->json($game, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        $game->delete();

        return response()->json(null, 204);
    }
}
