<?php

namespace App\Http\Controllers;

use App\Game;
use App\Rating;
use Illuminate\Http\Request;
use App\Http\Resources\Games as GameResource;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        
        return GameResource::collection($user->games()->paginate(40));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $userId = \Auth::user()->id;

        $rating = Rating::where('game_id', '=', $data['game_id'])->where('user_id', '=',  $userId)->first();
        if ($rating === null) {
            // user doesn't exist
            Rating::create(["game_id" => $data['game_id'], "user_id" => $userId, "rating" => $data['rating']]);
        } else {
            $rating->rating = $data['rating'];
            $rating->save();
        }
        // $raiting = Rating::updateOrCreate(["game_id" => $data['game_id'], "user_id" => $userId], ["rating" => $data['rating']]);
        $avg = Rating::where('game_id', $data['game_id'])->avg('rating');
        $game = Game::find($data['game_id']);
        // return response()->json($game, 201);
        $game->rating = $avg;
        $game->save();

        // return GameResource::collection(Game::where('id', $data['game_id'])->get());
        return response()->json($game, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Favourite  $favourite
     * @return \Illuminate\Http\Response
     */
    public function show(Favourite $favourite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Raiting  $raiting
     * @return \Illuminate\Http\Response
     */
    public function edit(Raiting $raiting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Raiting  $favourite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Raiting $raiting)
    {
        $data = $request->all();
        Rating::where('game_id', $data['game_id'])
          ->where('user_id', \Auth::user()->id)
          ->update(['rating' => $data['rating']]);
        $avg = Rating::where('game_id', $data['game_id'])->avg('rating');
        $game = Game::find($data['game_id']);
        $game->rating = $avg;
        $game->save();

        return response()->json($game, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Raiting  $favourite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->all();
        $userId = \Auth::user()->id;
        Raiting::where("game_id", $data['game_id'])->where("user_id", $userId)->delete();

        return response()->json(null, 204);
    }
}
