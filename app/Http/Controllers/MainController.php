<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use App\Category;
use Carbon\Carbon;

class MainController extends Controller
{
    public function index(){
        $path = storage_path() . "/json/results-27-05-18.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $json = json_decode(file_get_contents($path), true);
        // dd($json[0]['pageFunctionResult'][0]['categories']['category']);
        foreach ($json as $key => $j) {
            $game = new Game();
            // if(count($j["categories"]['category'] ) > 1) {
            //     foreach ($j["categories"]['category'] as $k => $value) {
            //         $res = Category::where('name', $value)->first();
            //         if(!$res){
            //             Category::create(['name' => $value]);
            //         }
            //     }
            // } else {
            //     $res = Category::where('name', $value)->first();
            //         if(!$res){
            //         Category::create(['name' => $j["categories"]['category']]);
            //     }
            // }
            // $game->$j["categories"];
            $game->buyitnow = $j["buyitnow"];
            $game->description = (isset($j["description"])) ? $j["description"] : '';
            $game->platform = (isset($j["platform"])) ? $j["platform"] : '';
            $game->publisher = (isset($j["publisher"])) ? $j["publisher"] : '';
            $game->developer = (isset($j["developer"])) ? $j["developer"] : '';
            $game->slug = $j["slug"];
            $game->release_date = Carbon::parse($j["release_date"]);
            $game->digitaldownload = $j["digitaldownload"];
            $game->free_to_start = $j["free_to_start"];
            $game->title = $j["title"];
            $game->system = $j["system"];
            $game->game_id = $j["id"];
            $game->number_of_players = $j["number_of_players"];
            // $game->release_date_display = $j["release_date_display"];
            $game->front_box_art = $j["front_box_art"];
            $game->game_code = $j["game_code"];
            $game->buyonline = $j["buyonline"];
            if(isset($j["eshop_price"])){
                $game->eshop_price = $j["eshop_price"];
            }
            if(isset($j["sale_price"])){
                $game->sale_price = $j["sale_price"];
            }
            $game->save();
            if( is_array($j["categories"]['category'] ) ) {
                foreach ($j["categories"]['category'] as $k => $value) {
                    $res = Category::where('name', $value)->first();
                    $game->categories()->attach($res->id);
                }
            } else {
                $res = Category::where('name', $value)->first();
                $game->categories()->attach($res->id);
            }
        }
        // dd($json);
    }
}
