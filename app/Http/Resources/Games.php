<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Games extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'title'             => $this->title,
            'description'       => $this->description,
            'number_of_players' => $this->number_of_players,
            'platform'          => $this->platform,
            'developer'         => $this->developer,
            'publisher'         => $this->publisher,
            'image'             => 'https://nintendo-sw-store.firebaseapp.com/images/games/' .$this->slug.'.png',
            'eshop_price'       => $this->eshop_price,
            'sale_price'        => $this->sale_price,
            'release_date'      => $this->release_date->format('m/d/Y'),
            'categories'        => array_column($this->categories->toArray(), 'name'),
            'favourite'         => $this->favourite(),
            'sales_end'         => is_null($this->sales_end) ? '' : $this->sales_end->format('m/d/Y'),
            'rating'            => $this->rating,
            'your_rating'       => $this->yourRating(),
            'slug'              => $this->slug
        ];
        // return parent::toArray($request);
    }
}
