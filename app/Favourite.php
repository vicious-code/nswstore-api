<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $table = "game_user";

    protected $fillable = ['game_id', 'user_id'];

    public $timestamps = false;
}
