<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Games routes
Route::resource('games', 'GameController')->middleware('auth:api');
Route::get('fullGames', 'GameController@fullGames')->middleware('auth:api');
Route::get('recentGames', 'GameController@recentGames')->middleware('auth:api');
Route::get('saleGames', 'GameController@saleGames')->middleware('auth:api');
Route::get('comingSoonGames', 'GameController@comingSoonGames')->middleware('auth:api');
// Favourites routes
Route::post('favourite-create', 'FavouriteController@store')->middleware('auth:api');
Route::delete('favourite-delete', 'FavouriteController@destroy')->middleware('auth:api');
Route::get('favourites', 'FavouriteController@index')->middleware('auth:api');
Route::get('ratings', 'RatingController@index')->middleware('auth:api');
Route::post('ratings-create', 'RatingController@store')->middleware('auth:api');
Route::delete('ratings-delete', 'RatingController@destroy')->middleware('auth:api');
// Users routes
Route::post('register','UsersController@create');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

